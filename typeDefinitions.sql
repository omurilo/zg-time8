CREATE TYPE dados_conciliacao AS (
	convenio varchar(255),
	data_pagamento date,
	numero_protocolo varchar(255),
	matricula varchar(255),
	nome varchar(255),
	numero_guia varchar(255),
	ng_prest varchar(255),
	senha_guia varchar(255),
	codigo_produto varchar(255),
	descricao_produto varchar(255),
	valor_apresentado numeric(19,2),
	valor_pago numeric(19,2),
	valor_glosa numeric(19,2),
	descricao_motivo varchar(255),
	codigo_motivo varchar(255)
);