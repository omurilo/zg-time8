import Selenium from "../libs/selenium";
import { DownloadLink } from "../models";

const CronJob = async () => {
  const covenants = await DownloadLink.findAll();

  for (const covenant of covenants) {
    // Hack to test one covenant
    // if (covenant.file_name_prefix === "pagatudo") {
    try {
      Selenium({
        url: covenant.link_downloads,
        format: covenant.file_date_format,
        prefix: covenant.file_name_prefix,
      });
      // Queues é um array com as intruções de job para baixar os arquivos e fazer a a conciliação.
      // Executar alguma coisa depois do download dos arquivos. To pensando em salvar eles no disco e ter um serviço
      // pra ler os arquivos e executar as transações
      // porque não da pra chamar um job a partir de outro job (esse código aqui é de um job então...)
    } catch (error) {
      console.log(error);
    }
    // }
  }
};

export default CronJob;
