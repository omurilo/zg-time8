import Bull from "bull";
import { redisConfig } from "../../config";

import Conciliate from "../jobs/conciliate";
import Download from "../jobs/download";
import ErrorMail from "../jobs/errorMail";

class Queue {
  get queues() {
    return [Conciliate, Download, ErrorMail].map(job => ({
      bull: new Bull(job.key, { redis: redisConfig }),
      name: job.key,
      handle: job.handle,
      options: job.options,
    }));
  }

  get bulls() {
    return this.queues;
  }

  add(name, data) {
    const queue = this.queues.find(queue => queue.name === name);

    return queue.bull.add(data, queue.options);
  }

  empty(name) {
    const queue = this.queues.find(queue => queue.name === name);

    return queue.bull.empty();
  }

  process() {
    return this.queues.forEach(queue => {
      queue.bull.process(4, queue.handle);

      queue.bull.on("failed", (job, err) => {
        console.log("Job failed", queue.name, job.data);
        console.log(err);
      });
    });
  }
}

export default new Queue();
