import dateFormat from "date-fns/format";
import { parseISO } from "date-fns";

import { DownloadedArchives } from "../models";
import Queue from "./queue";
import Parser from "../libs/parser";

const Conciliation = async ({ archiveName, archiveToParser }) => {
  /** Tentar procurar ou criar uma nova entrada no banco */
  const [archiveToDownload, created] = await DownloadedArchives.findOrCreate({
    where: { file_name: archiveName },
    defaults: {
      file_name: archiveName,
      read: true,
    },
  });

  const format = archiveName.split(".")[1];

  let obj = [];

  if (format === "xml") {
    const xml = await Parser.xml({
      xml: { name: archiveName, data: archiveToParser.data },
    });

    obj = [...obj, ...xml];
  } else if (format === "html") {
    const html = await Parser.html({
      html: { data: archiveToParser.data },
    });

    obj = [...obj, ...html];
  } else if (format === "csv") {
    const csv = await Parser.csv({
      csv: { data: archiveToParser.data },
    });

    obj = [...obj, ...csv];
  }

  const arrayArchive = [];

  try {
    if (obj.length) {
      for (const item of obj) {
        const date = parseISO(item.data_pagamento)
          ? parseISO(item.data_pagamento)
          : new Date(item.data_pagamento);

        const valorAp = Number(
          String(item.valor_apresentado).replace(/,/, ".")
        );

        const valorPago = Number(String(item.valor_pago).replace(/,/, "."));

        const valorGlosa = Number(String(item.valor_glosa).replace(/,/, "."));

        arrayArchive.push(
          `('${item.convenio}',
          '${dateFormat(date, "yyyy-MM-dd")}'::date,
          '${item.numero_protocolo}',
          '${item.matricula}',
          '${item.nome}',
          '${item.numero_guia}',
          '${item.ng_prest || item.numero_guia_prestador}',
          '${item.senha_guia || item.senha}',
          '${item.codigo_produto || item.codigo}',
          '${item.descricao_produto || item.descricao}',
          ${valorAp ? Number(valorAp.toFixed(2)) : 0}::numeric(19, 2),
          ${valorPago ? Number(valorPago.toFixed(2)) : 0}::numeric(19, 2),
          ${valorGlosa ? Number(valorGlosa.toFixed(2)) : 0}::numeric(19, 2),
          '${item.descricao_motivo}',
          '${item.codigo_motivo}')`
        );
      }

      if (!created) {
        DownloadedArchives.update(
          { read: true },
          { where: { id: archiveToDownload.id } }
        );
      }
    } else {
      throw new Error("não foi possível parsear o arquivo");
    }
  } catch (error) {
    /**  UPDATE read para false no banco para ler novamente amanhã */
    DownloadedArchives.update(
      { read: false },
      { where: { id: archiveToDownload.id } }
    );

    console.log(error);

    /** TODO: enviar e-mail de erro no arquivo */
    // Isso aqui não vai funcionar por enquanto;
    Queue.add("ErrorMail", { name: archiveName });
  }

  let arrayArchiveLength = arrayArchive.length;
  let oneQuarter = Math.floor(arrayArchiveLength * 0.01 * 25);

  if (oneQuarter > 750) {
    oneQuarter = 750;
  } else if (oneQuarter < 63) {
    oneQuarter = arrayArchiveLength;
  }

  while (arrayArchiveLength > 0) {
    const arrayToConciliate = arrayArchive.splice(0, oneQuarter);
    console.log(`added to conciliate: ${arrayToConciliate.length}`);
    Queue.add("Conciliate", {
      guia: arrayToConciliate,
      archiveName,
    });

    arrayArchiveLength -= oneQuarter;
  }
};

export default Conciliation;
