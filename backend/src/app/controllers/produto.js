import { Produto } from "../models";

export default {
  async store() {},
  async index(req, res) {
    try {
      const { limit, offset } = req.query;

      const produtos = await Produto.findAll({ limit, offset });
      // const guias = await ZG.query("SELECT * from guia LIMIT 15", {
      //   type: Sequelize.QueryTypes.SELECT,
      // });

      return res.json(produtos);
    } catch (error) {
      return res.status(500).send(`Ocorreu um erro: ${error}`);
    }
  },
  async show(req, res) {
    try {
      const { id } = req.params;
      const result = await Produto.findByPk(id);

      return res.json(result);
    } catch (error) {
      return res.status(error.status).send(`Ocorreu um erro: ${error}`);
    }
  },
  async update() {},
  async delete() {},
};
