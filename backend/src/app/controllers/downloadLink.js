import { DownloadLink } from "../models";

export default {
  async store(req, res) {
    const { data } = req.body;

    await DownloadLink.create({ ...data });

    return res.status(200).send();
  },

  async update(req, res) {
    try {
      return res.status(200).json();
    } catch (error) {
      return res.status(400).json({
        error: ``,
      });
    }
  },

  async show(req, res) {
    return res.status(200).json();
  },

  async index(req, res) {
    const links = await DownloadLink.findAll();

    return res.status(200).json(links);
  },

  async delete(req, res) {
    try {
      return res.status(200).json({ ok: true, msg: "" });
    } catch (error) {
      return res.status(400).json({ error: "" });
    }
  },
};
