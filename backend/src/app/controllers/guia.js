import { endOfDay, startOfDay } from "date-fns";
import { toDate } from "date-fns-tz";

import Models from "../models";
const {
  Guia,
  Beneficiario,
  Convenio,
  Prestador,
  QuitacaoGuia,
  Sequelize: { literal, Op },
} = Models;

export default {
  async store() {},
  async index(req, res) {
    try {
      const { limit, offset, textFilter, initialDate, endDate } = req.query;

      let startDay;
      let endDay;

      if (initialDate && endDate) {
        try {
          startDay = startOfDay(toDate(initialDate));
          endDay = endOfDay(toDate(endDate));
        } catch (error) {
          return res.status(400).json({
            error: `Initial or end date its an bad format: ${initialDate} - ${endDate}`,
          });
        }
      }

      const textFilterToWhere = textFilter
        ? {
            [Op.or]: [
              {
                numero: {
                  [Op.iLike]: `%${textFilter}`,
                },
              },
              literal(`"beneficiario"."nome" ILIKE '%${textFilter}%'`),
              literal(`"beneficiario"."matricula" ILIKE '%${textFilter}%'`),
            ],
          }
        : undefined;

      const dateFilter =
        !!startDay && !!endDay
          ? {
              data: {
                [Op.between]: [startDay, endDay],
              },
            }
          : undefined;

      const whereOptions = {
        ...dateFilter,
        ...textFilterToWhere,
      };

      const order = startDay && endDay ? ["data", "ASC"] : ["id", "DESC"];

      const { count, rows } = await Guia.findAndCountAll({
        where: whereOptions,
        order: [order],
        limit,
        offset,
        attributes: ["id", "numero", "data", "valor_total"],
        include: [
          {
            model: Beneficiario,
            as: "beneficiario",
            attributes: { exclude: ["version"] },
            required: true,
          },
          {
            model: Convenio,
            as: "convenio",
            attributes: { exclude: ["version"] },
            required: true,
          },
          {
            model: Prestador,
            as: "prestador",
            attributes: { exclude: ["version"] },
            required: true,
          },
          {
            model: QuitacaoGuia,
            as: "quitacao_guia",
            attributes: {
              exclude: ["version", "guia_id", "id"],
            },
          },
        ],
      });

      const guias = JSON.parse(JSON.stringify(rows)).map(guia => {
        return {
          ...guia,
          quitacao_guia: guia.quitacao_guia.reduce((acc, curr) => {
            acc = {
              data_pagamento: curr.data_pagamento,
              valor_pago: (
                parseFloat(acc.valor_pago ?? 0) +
                parseFloat(curr.valor_pago ?? 0)
              ).toFixed(2),
            };

            return acc;
          }, {}),
        };
      });

      return res.json({ totalItems: count, guias });
    } catch (error) {
      return res.status(500).send(`Ocorreu um erro: ${error}`);
    }
  },
  async show(req, res) {
    try {
      const { id } = req.params;
      const { limit, offset } = req.query;

      const result = await Guia.findByPk(id, {
        limit,
        offset,
        attributes: ["id", "numero", "data", "valor_total"],
        include: [
          {
            model: Beneficiario,
            as: "beneficiario",
            attributes: { exclude: ["version"] },
          },
          {
            model: Convenio,
            as: "convenio",
            attributes: { exclude: ["version"] },
          },
          {
            model: Prestador,
            as: "prestador",
            attributes: { exclude: ["version"] },
          },
          {
            model: QuitacaoGuia,
            as: "quitacao_guia",
            attributes: { exclude: ["version", "guia_id", "id"] },
          },
        ],
      });

      return res.json(result);
    } catch (error) {
      return res.status(error.status).send(`Ocorreu um erro: ${error}`);
    }
  },
  async update() {},
  async delete() {},
};
