export default (Sequelize, DataTypes) => {
  const QuitacaoGuia = Sequelize.define(
    "QuitacaoGuia",
    {
      id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
      },
      version: DataTypes.BIGINT,
      data_pagamento: DataTypes.DATE,
      valor_pago: DataTypes.DECIMAL(19, 2),
    },
    {
      tableName: "quitacao_guia",
    }
  );

  QuitacaoGuia.associate = models => {
    QuitacaoGuia.belongsTo(models.Guia, { as: "guia", foreignKey: "guia_id" });
  };

  return QuitacaoGuia;
};
