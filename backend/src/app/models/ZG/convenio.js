export default (Sequelize, DataTypes) => {
  const Convenio = Sequelize.define(
    "Convenio",
    {
      id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
      },
      version: DataTypes.BIGINT,
      ans: DataTypes.STRING(255),
      nome: DataTypes.STRING(255),
    },
    {
      tableName: "convenio",
    }
  );

  Convenio.associate = models => {
    Convenio.hasMany(models.Guia, { as: "guias", foreignKey: "convenio_id" });
  };

  return Convenio;
};
