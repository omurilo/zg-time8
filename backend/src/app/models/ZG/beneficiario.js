export default (Sequelize, DataTypes) => {
  const Beneficiario = Sequelize.define(
    "Beneficiario",
    {
      id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true,
      },
      version: DataTypes.BIGINT,
      matricula: DataTypes.STRING(255),
      nome: DataTypes.STRING(255),
    },
    {
      tableName: "beneficiario",
    }
  );

  Beneficiario.associate = models => {
    Beneficiario.hasMany(models.Guia, {
      as: "guias",
      foreignKey: "beneficiario_id",
    });
  };

  return Beneficiario;
};
