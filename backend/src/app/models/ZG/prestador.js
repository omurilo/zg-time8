export default (Sequelize, DataTypes) => {
  const Prestador = Sequelize.define(
    "Prestador",
    {
      id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
      },
      version: DataTypes.BIGINT,
      cnpj: DataTypes.STRING(255),
      nome: DataTypes.STRING(255),
    },
    {
      tableName: "prestador",
    }
  );

  Prestador.associate = models => {
    Prestador.hasMany(models.Guia, { as: "guias", foreignKey: "prestador_id" });
  };

  return Prestador;
};
