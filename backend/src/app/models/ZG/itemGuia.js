export default (Sequelize, DataTypes) => {
  const ItemGuia = Sequelize.define(
    "ItemGuia",
    {
      id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
      },
      version: DataTypes.BIGINT,
      quantidade: DataTypes.INTEGER,
      valor_total: DataTypes.DECIMAL(19, 2),
      numero: DataTypes.INTEGER,
    },
    {
      tableName: "item_guia",
    }
  );

  ItemGuia.associate = models => {
    ItemGuia.belongsTo(models.Guia, { as: "guia", foreignKey: "guia_id" });
    ItemGuia.belongsTo(models.Produto, {
      as: "produto",
      foreignKey: "produto_id",
    });
    ItemGuia.hasMany(models.QuitacaoItem, {
      as: "quitacao_items",
      foreignKey: "item_guia_id",
    });
  };

  return ItemGuia;
};
