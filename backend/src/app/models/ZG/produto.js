export default (Sequelize, DataTypes) => {
  const Produto = Sequelize.define(
    "Produto",
    {
      id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
      },
      version: DataTypes.BIGINT,
      codigo: DataTypes.STRING(255),
      nome: DataTypes.STRING(255),
      valor_unitario: DataTypes.DECIMAL(19, 2),
    },
    {
      tableName: "produto",
    }
  );

  Produto.associate = models => {
    Produto.hasMany(models.ItemGuia, {
      as: "produto",
      foreignKey: "produto_id",
    });
  };

  return Produto;
};
