export default (Sequelize, DataTypes) => {
  const QuitacaoItem = Sequelize.define(
    "QuitacaoItem",
    {
      id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
      },
      version: DataTypes.BIGINT,
      valor: DataTypes.DECIMAL(19, 2),
      motivo_glosa_descricao: DataTypes.STRING(255),
      motivo_glosa_codigo: DataTypes.STRING(255),
    },
    {
      tableName: "quitacao_item",
    }
  );

  QuitacaoItem.associate = models => {
    QuitacaoItem.belongsTo(models.ItemGuia, {
      as: "guias",
      foreignKey: "item_guia_id",
    });
  };

  return QuitacaoItem;
};
