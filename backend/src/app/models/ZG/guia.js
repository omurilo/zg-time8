export default (Sequelize, DataTypes) => {
  const Guia = Sequelize.define(
    "Guia",
    {
      id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true,
      },
      version: DataTypes.BIGINT,
      numero: DataTypes.STRING(255),
      valor_total: DataTypes.DECIMAL(19, 2),
      data: DataTypes.DATEONLY,
    },
    {
      tableName: "guia",
    }
  );

  Guia.associate = models => {
    Guia.belongsTo(models.Beneficiario, {
      as: "beneficiario",
      foreignKey: "beneficiario_id",
    });
    Guia.belongsTo(models.Convenio, {
      as: "convenio",
      foreignKey: "convenio_id",
    });
    Guia.belongsTo(models.Prestador, {
      as: "prestador",
      foreignKey: "prestador_id",
    });
    Guia.hasMany(models.ItemGuia, {
      as: "items",
      foreignKey: "guia_id",
    });
    Guia.hasMany(models.QuitacaoGuia, {
      as: "quitacao_guia",
      foreignKey: "guia_id",
    });
  };

  return Guia;
};
