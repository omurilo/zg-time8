"use strict";

import fs from "fs";
import path from "path";
import Sequelize from "sequelize";
import { databaseConfig } from "../../config";

const basename = path.basename(__filename);

const db = {};

const databases = Object.keys(databaseConfig);

for (const i in databases) {
  const database = databases[i];
  const dbPath = databaseConfig[database];
  // Store the database connection in our db object
  db[database] = new Sequelize(dbPath);
}

fs.readdirSync(path.join(__dirname, "ZG"))
  .filter(file => {
    return (
      file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".js"
    );
  })
  .forEach(file => {
    const model = db.ZG.import(path.join(__dirname, "ZG", file));
    const modelName = `${model.name[0].toUpperCase()}${model.name.slice(1)}`;
    db[modelName] = model;
  });

fs.readdirSync(path.join(__dirname, "LOCAL"))
  .filter(
    file =>
      file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".js"
  )
  .forEach(file => {
    const model = db.LOCAL.import(path.join(__dirname, "LOCAL", file));
    const modelName = `${model.name[0].toUpperCase()}${model.name.slice(1)}`;
    db[modelName] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.Sequelize = Sequelize;

export default db;
