export default (Sequelize, DataTypes) => {
  const DownloadLink = Sequelize.define("DownloadLink", {
    id: {
      type: DataTypes.BIGINT,
      autoIncrement: true,
      primaryKey: true,
    },
    convenio_id: DataTypes.BIGINT,
    link_downloads: DataTypes.STRING,
    file_date_format: DataTypes.STRING,
    file_name_prefix: DataTypes.STRING,
  });

  return DownloadLink;
};

/**
 * download_link
 * 
  id bigint NOT NULL,
    convenio_id bigint NOT NULL,
    link_downloads text COLLATE pg_catalog."default" NOT NULL,
    file_date_format character varying(255) COLLATE pg_catalog."default" NOT NULL,
    file_name_prefix character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT download_link_pkey PRIMARY KEY (id)
 * 
 */
