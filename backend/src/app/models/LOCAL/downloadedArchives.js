export default (Sequelize, DataTypes) => {
  const DownloadedArchives = Sequelize.define("DownloadedArchives", {
    id: {
      type: DataTypes.BIGINT,
      autoIncrement: true,
      primaryKey: true,
    },
    file_name: DataTypes.STRING,
    read: DataTypes.BOOLEAN,
  });

  return DownloadedArchives;
};

/**
  * arquivos_baixados
  *
  download_conf_id bigint NOT NULL,
    file_name character varying(255) NOT NULL,
    read_date timestamp with time zone,
    PRIMARY KEY (download_conf_id, file_name),
    CONSTRAINT "arquivos_baixados_download_link_FK" FOREIGN KEY (download_conf_id)
  */
