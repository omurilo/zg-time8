import axios from "axios";
import { Conciliation } from "../services";

class Download {
  get key() {
    return "Download";
  }

  get options() {
    return {
      attempts: 3,
      priority: 5,
    };
  }

  async handle(job, done) {
    const { archiveUrl, archiveName } = job.data;

    job.progress(33);

    const archiveToParser = await axios.get(archiveUrl);

    job.progress(66);

    Conciliation({ archiveName, archiveToParser });

    job.progress(100);

    return done();
  }
}

export default new Download();
