import { Mail } from "../services";

class ErrorMail {
  get key() {
    return "ErrorMail";
  }

  get options() {
    return {
      attempts: 3,
      priority: 2,
      delay: 2500,
    };
  }

  async handle(job, done) {
    const { name } = job.data;

    await Mail.sendMail({
      from: `"ZG Avisa" <avisa@zeroglosa.com.br>`,
      to: "Suporte <suporte@zeroglosa.com.br>",
      subject: `Erro no arquivo xml: ${name}`,
      template: "errorParser",
      context: { name },
    });

    return done();
  }
}

export default new ErrorMail();
