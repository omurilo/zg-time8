import Models from "../models";

const { ZG } = Models;

class Conciliate {
  get key() {
    return "Conciliate";
  }

  get options() {
    return {
      attempts: 3,
      priority: 4,
    };
  }

  async handle(job, done) {
    const { guia, archiveName } = job.data;

    let guiaLength = guia.length;
    const rate = guia.length * 0.01;
    const chunkLength = Math.floor(
      Math.random() * (guiaLength - (1 + guiaLength * 0.15) + 1) +
        (1 + guiaLength * 0.15)
    );

    while (guiaLength > 0) {
      const arrayToQuery = guia.splice(0, chunkLength);
      const res = await ZG.query(`SELECT public.realizar_conciliacao((SELECT ARRAY(SELECT aux::dados_conciliacao FROM (values
        ${arrayToQuery}) as aux)))`);

      guiaLength -= chunkLength;

      if (guiaLength < 0) guiaLength = 0;

      console.log(
        `enviadas ${arrayToQuery.length}`,
        `inseridas ${res[1].rows[0].realizar_conciliacao}`,
        archiveName
      );
      job.progress(Math.floor(100 - guiaLength / rate));
    }

    return done();
  }
}

export default new Conciliate();
