import { Builder, By, until } from "selenium-webdriver";
import firefox from "selenium-webdriver/firefox";

import { DownloadedArchives } from "../models";
import { Queue } from "../services";

async function Selenium({ url, format, prefix }) {
  const options = new firefox.Options();
  options.addArguments("-headless");

  const driver = new Builder()
    .forBrowser("firefox")
    .setFirefoxOptions(options)
    .build();

  try {
    await driver.get(url);
    const handleRootWindow = await driver.getWindowHandle();

    await driver
      .findElement(By.xpath('//*[@id="controllers"]/ul/li/a'))
      .click();

    const listArquivo = By.id("list-arquivo");
    await driver.wait(until.elementLocated(listArquivo), 3000);

    const content = await driver.findElement(listArquivo);
    const archives = await content.findElements(By.tagName("a"));

    const rows = await DownloadedArchives.findAll();

    console.time("queueToDownload");
    for (const a of archives) {
      const href = await a.getAttribute("href");
      const date = await a.getText();
      const archiveName = `${prefix}_${date}.${format}`;

      const row = rows.find(row => row.file_name === archiveName);

      if (row && row.read) {
        continue;
      } else if ((row && !row.read) || !row) {
        console.warn(
          `Eu não encontrei o arquivo (ou encontrei e ele não foi lido): ${archiveName} então vou baixá-lo`
        );

        await driver.switchTo().newWindow("tab");
        await driver.navigate().to(href);
        await driver.wait(until.elementLocated(By.id("list-arquivo")), 10000);
        await driver.sleep(1000);
        const archive = await driver.findElement(
          By.xpath('//*[@id="list-arquivo"]/div/ul/li/a')
        );

        /** Selecionar o link para guardar no banco e baixar */
        const archiveUrl = await archive.getAttribute("href");

        // add Queue to download archive;
        // console.log(this);
        Queue.add("Download", { archiveUrl, archiveName });
      }

      await driver.close();
      await driver.switchTo().window(handleRootWindow);
      await driver.sleep(1000);
    }

    console.timeEnd("queueToDownload");

    return driver.quit();
  } catch (e) {
    return console.log(e);
  }
}

export default Selenium;
