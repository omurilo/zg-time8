import XMLParser from "fast-xml-parser";
import cheerio from "cheerio";

export default {
  csv: ({ csv }) => {
    const lines = csv.data.split(/\r?\n/);
    const headers = lines[0].split(/\t|,/);

    const jsonResult = [];
    for (let i = 1; i < lines.length - 1; i++) {
      const currentLine = lines[i].split(/\t|,/);
      const jsonObject = {};
      for (let j = 0; j < headers.length; j++) {
        const propertyName = headers[j];

        const value = currentLine[j];
        jsonObject[propertyName] = value;
      }

      jsonResult.push(jsonObject);
    }

    return jsonResult;
  },
  xml: async ({ xml }) => {
    const {
      data: { row: resultArray },
    } = XMLParser.parse(xml.data);

    return resultArray;
  },
  html: ({ html }) => {
    const $ = cheerio.load(html.data);
    const getColHeadings = headingRow => {
      const alreadySeen = {};

      $(headingRow)
        .find("th")
        .each(function(j, cell) {
          let tr = $(cell)
            .text()
            .trim();

          if (alreadySeen[tr]) {
            const suffix = ++alreadySeen[tr];
            tr = `${tr}_${suffix}`;
          } else {
            alreadySeen[tr] = 1;
          }

          columnHeadings.push(tr);
        });
    };

    const processRow = (i, row) => {
      const rowJson = {};

      if (options.ignoreHeadingRow && i === options.rowForHeadings) return;

      $(row)
        .find("td")
        .each(function(j, cell) {
          rowJson[columnHeadings[j]] = $(cell)
            .text()
            .trim();
        });

      if (JSON.stringify(rowJson) !== "{}") jsonResponse.push(rowJson);
    };

    const options = {
      rowForHeadings: 0, // extract th cells from this row for column headings (zero-based)
      ignoreHeadingRow: true, // Don't tread the heading row as data
      ignoreRows: [],
    };
    const jsonResponse = [];
    const columnHeadings = [];

    $("table").each(function(i, table) {
      var trs = $(table).find("tr");

      // Set up the column heading names
      getColHeadings($(trs[options.rowForHeadings]));

      // Process rows for data
      $(table)
        .find("tr")
        .each(processRow);
    });

    return jsonResponse;
  },
};
