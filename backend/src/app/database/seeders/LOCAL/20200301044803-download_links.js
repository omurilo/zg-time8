"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("download_links", [
      {
        convenio_id: 655360,
        link_downloads: "https://glosamax.zeroglosa.com.br",
        file_date_format: "xml",
        file_name_prefix: "glosamax",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        convenio_id: 2,
        link_downloads: "https://glosamin.zeroglosa.com.br",
        file_date_format: "html",
        file_name_prefix: "glosamin",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        convenio_id: 1179648,
        link_downloads: "https://pagatudo.zeroglosa.com.br",
        file_date_format: "csv",
        file_name_prefix: "pagatudo",
        created_at: new Date(),
        updated_at: new Date()
      }
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("download_links", null, {});
  }
};
