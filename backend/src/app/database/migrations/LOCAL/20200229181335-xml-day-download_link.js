"use strict";

module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable("download_links", {
      id: {
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        type: DataTypes.BIGINT,
      },
      convenio_id: {
        allowNull: false,
        unique: true,
        type: DataTypes.BIGINT,
      },
      link_downloads: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      file_date_format: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      file_name_prefix: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    });
  },

  down: queryInterface => {
    return queryInterface.dropTable("download_links");
  },
};
