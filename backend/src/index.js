import server from "./server";

server.listen(process.env.PORT, () => {
  console.log(
    "\x1b[1m\x1b[5m\x1b[43m\x1b[37m%s\x1b[0m",
    `Listening backend in port ${process.env.PORT}`
  );
});
