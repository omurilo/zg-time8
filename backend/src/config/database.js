import * as dotenv from "dotenv";

dotenv.config({
  path: process.env.NODE_ENV === "test" ? ".env.test" : ".env",
});

const databaseConfig = {
  ZG: {
    host: process.env.DB_HOST_ZG,
    port: process.env.DB_PORT,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    dialect: "postgres",
    logging: process.env.NODE_ENV !== "production" && true,
    define: {
      freezeTableName: true,
      timestamps: false,
      underscored: true,
      underscoredAll: true,
    },
    pool: {
      acquire: 20000,
      evict: 15000,
      min: 2,
      max: 10,
      idle: 10000,
    },
  },
  LOCAL: {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    dialect: process.env.DB_DIALECT || "postgres",
    storage: "./__tests__/database.sqlite",
    logging: false,
    define: {
      timestamps: true,
      underscored: true,
      underscoredAll: true,
    },
  },
};

export default databaseConfig;
