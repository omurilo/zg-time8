export default {
  host: process.env.MAIL_HOST,
  port: process.env.MAIL_PORT,
  // service: process.env.MAIL_SERVICE,
  auth: {
    user: process.env.MAIL_USERNAME,
    pass: process.env.MAIL_PASSWORD,
  },
};
