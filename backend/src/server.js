import "dotenv/config";
import express from "express";
import BullBoard from "bull-board";
import cors from "cors";
import validate from "express-validation";
import Youch from "youch";
import cron from "node-cron";
import * as Sentry from "@sentry/node";

import Routes from "./routes";
import { sentryConfig } from "./config";
import { Queue, CronJob } from "./app/services";

class App {
  constructor() {
    this.express = express();
    this.isDev = process.env.NODE_ENV !== "production";

    this.sentry();
    this.middlewares();
    this.queues();
    this.routes();
    this.cron();
    this.exception();
  }

  queues() {
    if (this.isDev) {
      BullBoard.setQueues(Queue.bulls.map(queue => queue.bull));
      this.express.use("/admin/queues", BullBoard.UI);
    }
  }

  sentry() {
    Sentry.init({ dsn: sentryConfig.dsn });
  }

  middlewares() {
    this.express.use(express.json());
    this.express.use(cors());
    this.express.use(Sentry.Handlers.requestHandler());
  }

  routes() {
    this.express.use(Routes);
  }

  cron() {
    cron.schedule("59 22 */1 * *", () => {
      CronJob();
    });
  }

  exception() {
    if (!this.isDev) {
      this.express.use(Sentry.Handlers.errorHandler());
    }

    this.express.use(async (err, req, res) => {
      console.log(err);
      if (err instanceof validate.ValidationError) {
        return res.json(err);
      }

      if (this.isDev) {
        const youch = new Youch(err, req);

        return res.json(await youch.toJSON());
      }

      return res
        .status(err.status || 500)
        .json({ error: "Internal Server Error" });
    });
  }
}

export default new App().express;
