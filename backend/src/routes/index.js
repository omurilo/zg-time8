import express from "express";

import { CronJob } from "../app/services";

// import validate from "express-validation";
// import handler from "express-async-handler";

// import authMiddleware from "../middleware/auth";
// import validators from "../validators";

import {
  BeneficiarioController,
  ConvenioController,
  GuiaController,
  ItemGuiaController,
  PrestadorController,
  ProdutoController,
  QuitacaoGuiaController,
  QuitacaoItemController,
} from "../app/controllers";

const routes = new express.Router();

// OK
routes.get("/beneficiarios", BeneficiarioController.index);
routes.get("/beneficiarios/:id", BeneficiarioController.show);

// OK
routes.get("/convenios", ConvenioController.index);
routes.get("/convenios/:id", ConvenioController.show);

// OK
routes.get("/guias", GuiaController.index);
routes.get("/guias/:id", GuiaController.show);

//  OK
routes.get("/items-guia", ItemGuiaController.index);
routes.get("/items-guia/:id", ItemGuiaController.show);

// OK
routes.get("/prestadores", PrestadorController.index);
routes.get("/prestadores/:id", PrestadorController.show);

// OK
routes.get("/produtos", ProdutoController.index);
routes.get("/produtos/:id", ProdutoController.show);

// OK
routes.get("/quitacao-guias", QuitacaoGuiaController.index);
routes.get("/quitacao-guias/:id", QuitacaoGuiaController.show);

// OK
routes.get("/quitacao-items", QuitacaoItemController.index);
routes.get("/quitacao-items/:id", QuitacaoItemController.show);

routes.get("/cron", (req, res, next) => {
  CronJob();
  res.json({
    Hello: "How are you? This is an API for the conciliation process",
  });
});

export default routes;
