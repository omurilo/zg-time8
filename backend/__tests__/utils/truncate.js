import { LOCAL } from "../../src/app/models";

export default () => {
  return Promise.all(
    Object.keys(LOCAL.models).map(key => {
      return LOCAL.models[key].destroy({ truncate: true, force: true });
    })
  );
};
