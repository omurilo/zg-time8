import truncate from "./utils/truncate";

describe("StoreDownloadLink", () => {
  beforeEach(async () => {
    await truncate();
  });

  it("should create download link", async () => {});
});

describe("UpdateDownloadLink", () => {
  beforeEach(async () => {
    await truncate();
  });

  it("should update download link", async () => {});
});

describe("ShowDownloadLink", () => {
  beforeEach(async () => {
    await truncate();
  });

  it("should show download link", async () => {});
});

describe("IndexDownloadLink", () => {
  beforeEach(async () => {
    await truncate();
  });

  it("should index download link", async () => {});
});

describe("DeleteDownloadLink", () => {
  beforeEach(async () => {
    await truncate();
  });

  it("should delete download link", async () => {});
});
