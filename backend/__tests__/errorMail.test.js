import { Mail } from "../src/app/services";

describe("SendErrorMail", () => {
  it("should test sendMail function", async () => {
    const resp = await Mail.sendMail({
      from: "leandroppfteste@gmail.com",
      to: "leandroppf@gmail.com",
      subject: "Erro no arquivo xml: ",
      template: "errorParser",
      headers: {
        meetup: {
          user: {
            name: "Murilo Henrique",
          },
          title: "Aprendendo React Legal",
        },
        user: {
          name: "Diego Fernandes",
          email: "diego@rocketseat.com.br",
        },
      },
    })
      .then(resp => {
        console.log(resp);
      })
      .catch(err => {
        console.log(err);
      });

    expect(resp).toBe(resp);
  });
});
