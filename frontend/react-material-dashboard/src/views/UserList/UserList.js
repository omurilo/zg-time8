import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';

import { UsersToolbar, UsersTable } from './components';

import { getBeneficiaries } from '../../services/api';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  content: {
    marginTop: theme.spacing(2)
  }
}));

const GuideList = ( { match } ) => {
  const classes = useStyles();

  const [beneficiaries, setBeneficiaries] = useState([]);
  const [loading, setLoading] = useState(true);

  const handleBeneficiaries= async() =>{
    const beneficiariesList = await getBeneficiaries();
    await setBeneficiaries(beneficiariesList);
    setLoading(false);
  }

  useEffect(() => {
    handleBeneficiaries();
  }, [])

  console.log(beneficiaries)

  return (
    <div className={classes.root}>
      <UsersToolbar />
      <div className={classes.content}>
        {!loading && 
          (
            <UsersTable
              users={beneficiaries}
            />
          )
        }
      </div>
    </div>
  );
};

export default GuideList;
