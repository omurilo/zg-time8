import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';

import { getFinantialData  } from '../../services/api';

import {
  Budget,
  TotalUsers,
  TasksProgress,
  TotalProfit,
  LatestSales,
  UsersByDevice,
  LatestProducts,
  LatestOrders
} from './components';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4)
  }
}));

const Dashboard = () => {
  const classes = useStyles();

  
  const [finantial, setFinantial] = useState([]);
  const [loading, setLoading] = useState(true);


  const handelFinantial = async() =>{
    const finantialData = await getFinantialData();
    await setFinantial(finantialData);
    setLoading(false);
  }

  useEffect(() => {
    handelFinantial();
  }, [])

  

  return (
    <div className={classes.root}>
      {loading ? '' : (
        <Grid
        container
        spacing={4}
      >
        <Grid
          item
          lg={4}
          sm={6}
          xl={3}
          xs={12}
        >
          <Budget data={finantial.total.glosaTotal} />
        </Grid>
        <Grid
          item
          lg={4}
          sm={6}
          xl={3}
          xs={12}
        >
          <Budget data={finantial.total.totalPago} />
        </Grid>
        <Grid
          item
          lg={4}
          sm={6}
          xl={3}
          xs={12}
        >
          <Budget data={finantial.total.totalFaturado} />
        </Grid>
        <Grid
          item
          lg={12}
          md={12}
          xl={9}
          xs={12}
        >
          <LatestSales />
        </Grid>
        
      </Grid>
      )}
    </div>
  );
};

export default Dashboard;
