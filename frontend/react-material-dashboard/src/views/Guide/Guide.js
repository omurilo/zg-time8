import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';

import { UsersToolbar, UsersTable } from './components';

import { getGuia } from '../../services/api';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  content: {
    marginTop: theme.spacing(2)
  }
}));

const Guide = ( { match } ) => {
  const classes = useStyles();

  const [guide, setGuide] = useState([]);
  const [loading, setLoading] = useState(true);


  const handleGuides = async() =>{
    const guide_item = await getGuia(match.params.id);
    await setGuide(guide_item);
    setLoading(false);
  }

  useEffect(() => {
    handleGuides();
  }, [])
  console.log(guide)
  return (
    <div className={classes.root}>
      <UsersToolbar />
      <div className={classes.content}>
        {!loading && 
          (
            <UsersTable
              users={guide.items}
            />
          )
        }
      </div>
    </div>
  );
};

export default Guide;