import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';
import { IconButton, Grid, Typography } from '@material-ui/core';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import { Link } from 'react-router-dom';

import { ConventsToolbar, ConventCard } from './components';
import mockData from './data';

import { getConveniado } from '../../services/api';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  content: {
    marginTop: theme.spacing(2)
  },
  pagination: {
    marginTop: theme.spacing(3),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end'
  }
}));

const Covenants = () => {
  const classes = useStyles();

  const [conveniados, setConveniados] = useState([]);


  const handleConveniado = async() => {
    const convs = await getConveniado();
    setConveniados(convs);
  }

  useEffect(()=>{
    handleConveniado();
  }, [])

  return (
    <div className={classes.root}>
      <ConventsToolbar />
      <div className={classes.content}>
        <Grid
          container
          spacing={3}
        >
          {conveniados.map(product => (
            <Grid
              item
              key={product.id}
              lg={4}
              md={6}
              xs={12}
            >
              <Link to={`/convenios/${product.id}/guides`}>
                <ConventCard product={product} />
              </Link>
            </Grid>
          ))}
        </Grid>
      </div>
      <div className={classes.pagination}>
        <Typography variant="caption">1-6 of 20</Typography>
        <IconButton>
          <ChevronLeftIcon />
        </IconButton>
        <IconButton>
          <ChevronRightIcon />
        </IconButton>
      </div>
    </div>
  );
};

export default Covenants;
