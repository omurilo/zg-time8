import uuid from 'uuid/v1';

export default [
  {
    id: uuid(),
    title: 'Glosa Max',
    description:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed fringilla mauris vel erat dictum.',
    imageUrl: '/images/products/product_1.png',
    totalDownloads: '594',
    updatedAt: '27/03/2019'
  },
  {
    id: uuid(),
    title: 'Glosa Min',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed fringilla mauris vel erat dictum.',
    imageUrl: '/images/products/product_2.png',
    totalDownloads: '625',
    createdAt: '31/03/2019'
  },
  {
    id: uuid(),
    title: 'Paga Tudo',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed fringilla mauris vel erat dictum.',
    imageUrl: '/images/products/product_3.png',
    totalDownloads: '857',
    createdAt: '03/04/2019'
  },
];
