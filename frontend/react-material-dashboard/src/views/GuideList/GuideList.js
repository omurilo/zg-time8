import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';

import { UsersToolbar, UsersTable } from './components';
import mockData from './data';

import { getGuides } from '../../services/api';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  content: {
    marginTop: theme.spacing(2)
  }
}));

const GuideList = ( { match } ) => {
  const classes = useStyles();

  const [guides, setGuides] = useState([]);
  const [loading, setLoading] = useState(true);

  const handleGuides = async() =>{
    const guideList = await getGuides(match.params.id);
    await setGuides(guideList);
    setLoading(false);
  }

  useEffect(() => {
    handleGuides();
  }, [])

  console.log(guides)

  return (
    <div className={classes.root}>
      <UsersToolbar />
      <div className={classes.content}>
        {!loading && 
          (
            <UsersTable
              users={guides.list}
            />
          )
        }
      </div>
    </div>
  );
};

export default GuideList;
