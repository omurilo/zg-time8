import axios from 'axios';
import Config from './configAPI';

console.log(Config.Url);

export function getGuides(idConvenio) {
  return axios({
    method: 'GET',
    url: `${Config.Url}get-guides/${idConvenio}`,
  }).then((response) => response.data)
    .catch((error) => {
      throw error;
    });
};

export function getGuia(idGuia) {
  return axios({
    method: 'GET',
    url: `${Config.Url}get-guia/${idGuia}`,
    params: {
      idGuia,
    },
  }).then((response) => response.data)
    .catch((error) => {
      throw error;
    });
};

export function getConvenio(idConvenio) {
  return axios({
    method: 'POST',
    url: `${Config.url}get-convenio`,
    params: {
      idConvenio
    },
  }).then((response) => response.data)
    .catch((error) => {
      throw error;
    });
};

export function getConveniado() {
  return axios({
    method: 'GET',
    url: `${Config.Url}get-convenants`,
  }).then(response => response.data)
    .catch((error) => {
      throw error;
    });
};

export function getFinantialData() {
  return axios({
    method: 'GET',
    url: `${Config.Url}get-financial-data`,
  }).then(response => response.data)
    .catch((error) => {
      throw error;
    });
};


export function getBeneficiaries() {
  return axios({
    method: 'GET',
    url: `${Config.Url}get-beneficiaries`,
  }).then(response => response.data)
    .catch((error) => {
      throw error;
    });
}

export function createConvenio(idConvenio) {
  return axios({
    method: 'POST',
    url:`${Config.url}create-convenio`,
    params: { 
      idConvenio 
    }
  }).then(response => response.data)
    .catch((error) => {
      throw error;
    });
};

export function createConveniado(idConveniado) {
  return axios({
    method: 'POST',
    url: `${Config.url}create-conveniado`,
    params: { 
      idConveniado 
    }
  }).then(response => response.data)
    .catch((error) => {
      throw error;
    });
};



export function createPrestador(idPrestador) {
  return axios({
    method: 'POST',
    url: `${Config.url}createrestador`,
    params: { 
      idPrestador 
    }
  }).then(response =>  response.data)
    .catch((error) => {
      throw error;
    });
};

export function updateGuia(idGuia) {
  return axios({
    method: 'PUT',
    url: `${Config.url}update-guia`,
    params: { 
      idGuia 
    }
  }).then(response => response.data)
    .catch((error) => {
      throw error;
    });
};

export function updateConvenio(idConvenio) {
  return axios({
    method: 'PUT',
    url: `${Config.url}update-convenio`,
    params: { 
      idConvenio 
    }
  }).then(response =>  response.data)
    .catch((error) => {
      throw error;
    })
}
