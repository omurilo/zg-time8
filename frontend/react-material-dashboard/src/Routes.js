import React from 'react';
import { Switch, Redirect } from 'react-router-dom';

import { RouteWithLayout } from './components';
import { Main as MainLayout, Minimal as MinimalLayout } from './layouts';

import {
  Dashboard as DashboardView,
  UserList as UserListView,
  NotFound as NotFoundView,
  Covenants as CovenantsView,
  GuideList as GuideListView,
  Guide as GuideView
} from './views';

const Routes = () => {
  return (
    <Switch>
      <Redirect
        exact
        from="/"
        to="/dashboard"
      />
      <RouteWithLayout
        component={CovenantsView}
        exact
        layout={MainLayout}
        path="/convenios"
      />
      <RouteWithLayout
        component={GuideListView}
        exact
        layout={MainLayout}
        path="/convenios/:id/guides"
      />
      <RouteWithLayout
        component={GuideView}
        exact
        layout={MainLayout}
        path="/guide/:id"
      />
      <RouteWithLayout
        component={DashboardView}
        exact
        layout={MainLayout}
        path="/dashboard"
      />
      <RouteWithLayout
        component={UserListView}
        exact
        layout={MainLayout}
        path="/users"
      />
      <RouteWithLayout
        component={NotFoundView}
        exact
        layout={MinimalLayout}
        path="/not-found"
      />
      <Redirect to="/not-found" />
    </Switch>
  );
};

export default Routes;
