
CREATE OR REPLACE FUNCTION listar_quitacoes_guia(input_guia_id bigint default NULL, max_results bigint default 100, first_result bigint default 0) returns table(
	"guiaId" bigint,
	"valorTotal" numeric(19,2),
	"valorQuitado" numeric(19,2),
	"glosa" numeric(19,2),
	"numItensTotal" bigint,
	"numItensGlosa" bigint,
	"numItensNaoEnviados" bigint,
	"numero" varchar(255),
	"prestadorId" bigint,
	"prestadorNome" varchar(255),
	"beneficiarioId" bigint,
	"beneficiarioNome" varchar(255),
	"beneficiarioMatricula" varchar(255)
) AS $$
	WITH guias(id, numero, prestador_id, prestador_nome, beneficiario_id, beneficiario_nome, beneficiario_matricula, convenio_id, convenio_nome, valor_total) as (
		SELECT 
		g.id,
		g.numero,
		p.id,
		p.nome,
		b.id,
		b.nome,
		b.matricula,
		c.id,
		c.nome,
		g.valor_total
		FROM guia g
		INNER JOIN prestador p ON p.id = g.prestador_id
		INNER JOIN beneficiario b ON b.id = g.beneficiario_id
		INNER JOIN convenio c ON c.id = g.convenio_id
		ORDER BY g.id desc
		LIMIT $2
		OFFSET $3
	),aux_quitacao_guia(guia_id, valor_pago) as (
		SELECT 
			g.id,
			COALESCE(SUM(qg.valor_pago), 0)
		FROM guias g 
		LEFT JOIN quitacao_guia qg ON g.id = qg.guia_id
		WHERE $1 ISNULL OR g.id = $1
		GROUP BY g.id
		LIMIT $2
	),
	aux_quitacao_item_guia(guia_id, num_itens_glosa, num_itens_nao_enviados, num_itens_total) as (
		SELECT
		ig.guia_id,
		COALESCE(SUM(CASE WHEN qi.motivo_glosa_codigo NOTNULL THEN 1 ELSE 0 END), 0),
		COALESCE(SUM(CASE WHEN qi.id ISNULL THEN 1 ELSE 0 END), 0),
		COUNT(DISTINCT ig.id)
		FROM aux_quitacao_guia aq
		INNER JOIN item_guia ig ON ig.guia_id = aq.guia_id
		LEFT JOIN quitacao_item qi ON qi.item_guia_id = ig.id
		GROUP BY ig.guia_id
		ORDER BY ig.guia_id 
		LIMIT $2
	)
	SELECT
	g.id AS guia_id,
	g.valor_total AS valor_total,
	COALESCE(qg.valor_pago, 0) AS valor_quitado,
	g.valor_total - COALESCE(qg.valor_pago, 0) AS glosa,
	qi.num_itens_total AS num_itens_total,
	qi.num_itens_glosa AS num_itens_glosa,
	qi.num_itens_total - qi.num_itens_glosa as num_itens_nao_enviados,
	g.numero,
	g.prestador_id,
	g.prestador_nome,
	g.beneficiario_id,
	g.beneficiario_nome,
	g.beneficiario_matricula
	FROM
	aux_quitacao_guia qg
	INNER JOIN guias g on g.id = qg.guia_id
	INNER JOIN aux_quitacao_item_guia qi ON g.id = qi.guia_id
	WHERE $1 ISNULL OR g.id = $1
	LIMIT $2;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION listar_conveniados(max_results bigint default 100, first_result bigint default 0) returns table(
	"beneficiarioId" bigint,
	"beneficiarioNome" varchar(255),
	"beneficiarioMatricula" varchar(255),
	"convenioId" bigint,
	"convenioNome" varchar(255)
) as $$
with aux_guia(id) as (
	SELECT
	max(g.id)
	FROM guia g
	INNER JOIN beneficiario b ON b.id = g.beneficiario_id
	GROUP BY b.id
	LIMIT $1
	OFFSET $2
)
SELECT
DISTINCT
b.id,
b.nome,
b.matricula,
c.id,
c.nome
FROM
aux_guia ag
INNER JOIN guia g ON g.id = ag.id
INNER JOIN beneficiario b ON b.id = g.beneficiario_id
INNER JOIN convenio c ON c.id = g.convenio_id;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION listar_dados_item_guia(guia_id bigint, max_results bigint default null, first_result bigint default 0) returns table(
	"numeroItem" integer,
	"produtoNome" varchar(255),
	"valorPago" numeric(19,2),
	"valorGlosa" numeric(19,2),
	"descricaoMotivo" varchar(255),
	"codigoMotivo" varchar(255),
	"quitacaoId" bigint
) as $$
SELECT
ig.numero,
p.nome,
COALESCE(qi.valor, 0),
ig.valor_total - COALESCE(qi.valor, 0),
qi.motivo_glosa_descricao,
qi.motivo_glosa_codigo,
qi.id
FROM item_guia ig
INNER JOIN produto p ON ig.guia_id = $1 AND ig.produto_id = p.id
LEFT JOIN quitacao_item qi ON qi.item_guia_id = ig.id
GROUP BY ig.id, p.nome, qi.id
ORDER BY ig.numero ASC
LIMIT $2
OFFSET $3;
$$ LANGUAGE SQL;

CREATE FUNCTION listar_glosa_prestadores(max_results bigint default 100, first_result bigint default 0) returns table(
	"prestadorId" bigint,
	"prestadorNome" varchar(255),
	"prestadorCnpj" varchar(255),
	"glosa" numeric(19,2),
	"valorTotal" numeric(19,2)
) as $$
WITH aux_guia(id, valor_total) as 
(
SELECT 
	p.id,
	COALESCE(SUM(g.valor_total),0)
	FROM 
	prestador p
	INNER JOIN guia g ON g.prestador_id = p.id
	GROUP BY p.id
	ORDER BY p.nome asc
	LIMIT $1
	OFFSET $2
),
aux_quitacao_guia(id, valor_quitado) as
(
SELECT
	g.prestador_id,
	COALESCE(SUM(qg.valor_pago))
	FROM
	aux_guia ag
	INNER JOIN guia g ON g.prestador_id = ag.id
	LEFT JOIN quitacao_guia qg ON qg.guia_id = g.id
	GROUP BY g.prestador_id
)
SELECT
	p.id,
	p.nome,
	p.cnpj,
	ag.valor_total,
	ag.valor_total - aq.valor_quitado
FROM
aux_guia ag
INNER JOIN aux_quitacao_guia aq ON aq.id = ag.id
INNER JOIN prestador p ON p.id = ag.id;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION listar_valores_convenios(max_results bigint default 100, first_result bigint default 0) returns table(
	"convenioId" bigint,
	"convenioNome" varchar(255),
	"convenioAns" varchar(255),
	"valorTotal" numeric(19,2),
	"valorPago" numeric(19,2)
) as $$
WITH aux_guia(id, valor_total) as (
SELECT
	c.id,
	SUM(g.valor_total)
	FROM guia g
	INNER JOIN convenio c ON c.id = g.convenio_id
	GROUP BY c.id
	LIMIT $1
	OFFSET $2
),
aux_quitacao_guia(id, valor_pago) as (
SELECT
	ag.id,
	SUM(qg.valor_pago)
	FROM aux_guia ag
	INNER JOIN guia g ON g.convenio_id = ag.id
	INNER JOIN quitacao_guia qg ON qg.guia_id = g.id
	GROUP BY ag.id
)
SELECT 
c.id,
c.nome,
c.ans,
COALESCE(ag.valor_total, 0),
COALESCE(aq.valor_pago, 0)
from aux_guia ag
LEFT JOIN aux_quitacao_guia aq ON aq.id = ag.id
RIGHT JOIN convenio c ON c.id = ag.id;
$$ LANGUAGE SQL;

-- TYPE 1

-- FUNCTION: public.realizar_conciliacao(dados_conciliacao[])

-- DROP FUNCTION public.realizar_conciliacao(dados_conciliacao[]);

CREATE OR REPLACE FUNCTION public.realizar_conciliacao(
	dados dados_conciliacao[])
    RETURNS TABLE(
			convenio character varying,
		-- data_pagamento date,
			numero_protocolo character varying,
			matricula character varying,
			nome character varying,
		--	numero_guia character varying, 
		-- ng_prest character varying,
			senha_guia character varying,
		-- codigo_produto character varying,
			descricao_produto character varying,
		-- valor_apresentado numeric,
		-- valor_pago numeric,
		-- valor_glosa numeric,
		-- descricao_motivo character varying,
		-- codigo_motivo character varying
		) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
	aux_item item_guia;
	aux_item_guia item_guia[];
	dado dados_conciliacao;
BEGIN
	FOREACH dado IN ARRAY dados
	LOOP
		aux_item_guia := (
			SELECT ARRAY (
				SELECT ig
				FROM item_guia ig 
				INNER JOIN guia g ON (g.numero = dado.numero_guia OR g.numero = dado.ng_prest) AND ig.guia_id = g.id
				INNER JOIN produto pd ON pd.id = ig.produto_id AND pd.codigo = dado.codigo_produto
			)
		);
		IF aux_item_guia[1] NOTNULL
		THEN
			FOREACH aux_item IN ARRAY aux_item_guia
			LOOP
				INSERT INTO quitacao_guia(id, version, guia_id, data_pagamento, valor_pago) values(
					nextval('hibernate_sequence'), 
					0, 
					aux_item.guia_id,
					dado.data_pagamento, 
					COALESCE(dado.valor_pago, COALESCE(dado.valor_apresentado, aux_item.valor_total) - dado.valor_glosa)
				);
				INSERT INTO quitacao_item(id, version, valor, motivo_glosa_descricao, item_guia_id, motivo_glosa_codigo) values(
					nextval('hibernate_sequence'), 
					0, 
					COALESCE(dado.valor_pago, COALESCE(dado.valor_apresentado, aux_item.valor_total) - dado.valor_glosa),
					COALESCE(dado.descricao_motivo, 'DESCONHECIDO'),
					aux_item.id,
					COALESCE(dado.codigo_motivo, '-1')
				);
			END LOOP;
		ELSE
			RETURN NEXT;
		END IF;
	END LOOP;
END;
$BODY$;

ALTER FUNCTION public.realizar_conciliacao(dados_conciliacao[])
    OWNER TO time8;